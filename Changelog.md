vtxNG Changelog
===============


* Core system
  * Contains:
    * Multilingual (i18n)
      * English & German included
      * Language schwitcher
      * Country Flags
    * Basic Standard pages
      * About Us
      * Disclaimer
      * Terms
      * Imprint
      * Privacy Policy
      * Contact
    * Bootstrap
    * JQuery
    * User login
    * User register
    * Admin area
    * EU Cookie Law conform

* Blog System
  * simular to Symblog
  
* Routing Change
  * Now routes go through _src/AppBundle/Resources/config/routing.yml_
  * NOTE: It is now yaml!

* EasyAdmin configuration
  * is now in _app/config/admin.yml_
  
