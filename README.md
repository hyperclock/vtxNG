vtxNG
=====

A Symfony project created on March 26, 2017, 6:06 pm.

vtxNG is our codename for the VoyaTrax Next Generation CMS. 

We will create a **FULL** *Blogging* system as a base system. It will have these
features:

  * Multilingual (i18n)
  * English & German included
  * Language schwitcher
  * Country Flags
  * Basic Standard pages
   * About Us
   * Disclaimer
   * Terms
   * Imprint
   * Privacy Policy
   * Contact
  * Bootstrap
  * JQuery
  * User login
  * User register
  * Admin area
  * EU Cookie Law conform
 
  
**more plans are being developed, for instance:**
  
  * Captcha / reCaptcha
  * Newsletter
  * RSS
  * Ability to get updates, like Bolt CM or WordPress
  
## LICENSE

[MIT License](LICENSE)


## Documentation

There is not a lot of docs yet. We are working on those as is needed. As far the building part goes, we will 
document that within the [wiki](wikis/home).


## Main Site

The [main website](http://www.voyatrax.eu) will be setup using vtxNG as soon as we are ready for testing.
This will happen, even if not ready for releasing.


#### How to Use

We're still in development, so we don't know how we will deploy this. Until then you could get it like this on a development server (pc?):

`git clone https://gitlab.com/hyperckock/vtxNG.git vtxNG`

**IMPORTANT!!!** Fix File Permissions (if you are not using Linux check the Symfony instructions). First make sure to change into the project’s directory:

`cd vtxNG`

Then fix the permissions like this:

```
HTTPDUSER=`ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
 
# if this doesn't work, try adding `-n` option
sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var
sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var
```

The next part will ask for Databank Info (you should already have them) and at the end it will ask for setting your "secret". You can get an [Online Generated key here](http://nux.net/secret).

`composer install`

answer any questions and it will or may end with errors message about an unkown databank. Fix that error with this:

`php bin/console doctrine:database:create`

<!--And then to make sure we got erverything the first time, we run the _install_ command again:

`composer install`-->


Then Create the database schema

`php bin/console doctrine:schema:create`


And now update your database:

`php bin/console doctrine:schema:update --force`


Then create your :Admin User_ (Replace _"Admin"_ with your adminuser name):

`php bin/console fos:user:create Admin --super-admin`


You need to update the fixtures

`php bin/console doctrine:fixtures:load`


And then we run this to update everything:

`composer update --with-dependencies`


_I HAVE NOT TESTED THIS YET. If you do please post an issue and let us know._


## See a screenshot...

[A screenshot is not available at the moment....](#)
