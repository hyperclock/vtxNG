<?php
// src/AppBundle/Controller/PageController.php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
// Import new namespaces
use AppBundle\Entity\Contact;
use AppBundle\Form\ContactType;

class PageController extends Controller {

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()
                   ->getManager();
 
        $blogs = $em->getRepository('AppBundle:Blog')
                    ->getLatestBlogs();
 
        return $this->render('AppBundle:Page:index.html.twig', array(
            'blogs' => $blogs
        ));
    }

    /**
     * @Route("/about", name="about")
     */
    public function aboutAction() {
        return $this->render('AppBundle:Page:about.html.twig');
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contactAction(Request $request)
    {
        $enquiry = new Contact();
        $form = $this->createForm(ContactType::class, $enquiry);

         $this->request = $request;
            if ($request->getMethod() == 'POST') {
            $form->bind($request);


    if ($form->isValid()) {


                $message = \Swift_Message::newInstance()
                    ->setSubject('Contact enquiry from Example')
                    ->setFrom('contact@example.com')
                    ->setTo($this->container->getParameter('app.emails.contact_email'))
                    ->setBody($this->renderView('AppBundle:Contact:contactEmail.txt.twig', array('enquiry' => $enquiry)));
                $this->get('mailer')->send($message);

            $this->get('session')->getFlashbag('blog-notice', 'Your contact enquiry was successfully sent. Thank you!');


        // Redirect - This is important to prevent users re-posting
        // the form if they refresh the page
        return $this->redirect($this->generateUrl('contact'));
    }
        }

        return $this->render('AppBundle:Contact:contact.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/search", name="search")
     */
    public function searchAction() {
        return $this->render('AppBundle:Page:search.html.twig');
    }

    /**
     * @Route("/terms", name="terms")
     */
    public function termsAction() {
        return $this->render('AppBundle:Page:terms.html.twig');
    }

    /**
     * @Route("/disclaimer", name="disclaimer")
     */
    public function disclaimerAction() {
        return $this->render('AppBundle:Page:disclaimer.html.twig');
    }
    
    /**
     * @Route("/privacy", name="privacy")
     */
    public function privacyAction() {
        return $this->render('AppBundle:Page:privacy.html.twig');
    }
    
    /**
     * @Route("/trademarks", name="trademarks")
     */
    public function trademarksAction() {
        return $this->render('AppBundle:Page:trademarks.html.twig');
    }
    
    /**
     * @Route("/imprint", name="imprint")
     */
    public function imprintAction() {
        return $this->render('AppBundle:Page:imprint.html.twig');
    }

    /**
     * @Route("/sidebar", name="sidebar")
     */
    public function sidebarAction()
    {
        $em = $this->getDoctrine()
                   ->getEntityManager();
 
        $tags = $em->getRepository('AppBundle:Blog')
                   ->getTags();
 
        $tagWeights = $em->getRepository('AppBundle:Blog')
                         ->getTagWeights($tags);
 
        $commentLimit   = $this->container
                               ->getParameter('app.comments.latest_comment_limit');
        $latestComments = $em->getRepository('AppBundle:Comment')
                             ->getLatestComments($commentLimit);
 
        return $this->render('AppBundle:Sidebar:sidebar.html.twig', array(
            'latestComments'    => $latestComments,
            'tags'              => $tagWeights
        ));
    }

}
